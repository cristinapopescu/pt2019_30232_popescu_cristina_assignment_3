package bl;
import dal.*;

public class ClientServices {
    private ClientDAL clientDAL = new ClientDAL();

    public void addClient(int cnp, String name){
        clientDAL.insertClient(name, cnp);
    }

    public void deleteClient(int cnp){
        clientDAL.deleteClient(cnp);
    }

    public void updateClient(int cnp, String name) {
        clientDAL.updateClient(name, cnp);
    }

    public Object[][] showClients() {
        return clientDAL.showClients();
    }
}
