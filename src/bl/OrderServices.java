package bl;

import dal.OrderDAL;

public class OrderServices {
    private OrderDAL orderDAL = new OrderDAL();

    public void insertOrder(int id, int product, int client, int quantity){
        orderDAL.insertOrder(id, product, client, quantity);
    }
}
