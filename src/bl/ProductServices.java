package bl;

import dal.ProductDAL;

public class ProductServices {
    private ProductDAL productDAL = new ProductDAL();

    public void addProduct(int id, String name, int quantity, double price){
        productDAL.insertProduct(id, name, quantity, price);
    }

    public void deleteProduct(int id){
        productDAL.deleteProduct(id);
    }

    public void updateProduct(int id, String name, int quantity, double price) {
        productDAL.updateProduct(id,name,quantity,price);
    }

    public void showProducts() {
        productDAL.showProducts();
    }
}
