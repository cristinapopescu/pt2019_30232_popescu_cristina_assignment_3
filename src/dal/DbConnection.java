package dal;
import java.lang.*;
import java.util.logging.Logger;
import java.sql.*;

public class DbConnection {

   private static final Logger LOGGER = Logger.getLogger(DbConnection.class.getName());
   private static final String DBURL = "jdbc:mysql://localhost:3306/tpdb";
   private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
   private static final String USER = "root";
   private static final String PASS = "root";

   private static DbConnection singleInstance = new DbConnection();

   private DbConnection() {
      try {
         Class.forName(DRIVER);
      } catch (ClassNotFoundException e) {
         e.printStackTrace();
      }
   }


   public static Connection getConnection() {
      Connection connection = null;
      try {
         connection = DriverManager.getConnection(DBURL, USER, PASS);
      } catch (SQLException e) {
         System.err.println("Connection failed!");
      }
      return connection;
   }

   public static void close(Connection connection) {

   }

   public static void close(Statement statement) {

   }

   public static void close(ResultSet resultSet) {

   }

}
