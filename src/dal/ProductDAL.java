package dal;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class ProductDAL {

    public ProductDAL() {

    }

    public void insertProduct(int id, String name, int quantity, double price) {
        try {

            Connection conn = DbConnection.getConnection();
            Statement stm = conn.createStatement();
            stm.execute("INSERT INTO product (id, name, quantity, price) VALUES ('" + id + "'" + "," + " '" + name + "'" + "," + " '" + quantity + "'" + "," + " '" + price + "')");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateProduct(int id, String name, int quantity, double price) {
        try {

            Connection conn = DbConnection.getConnection();
            Statement stm = conn.createStatement();
            stm.execute("UPDATE product SET id = '" + id + "', name = '" + name + "', quantity = '" + quantity + "', price = '" + price +"' WHERE id = '" + id + "'" );

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteProduct(int id) {
        try {

            Connection conn = DbConnection.getConnection();
            Statement stm = conn.createStatement();
            stm.execute("DELETE FROM product WHERE id = '" + id + "'");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Object[][] showProducts(){
        Object[][] data = new Object[10][10];

        try {

            Connection conn = DbConnection.getConnection();
            Statement stm = conn.createStatement();
            stm.execute("SELECT * FROM product");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }
}
