package dal;

import model.Client;

import java.sql.Connection;
import java.sql.*;
import java.util.ArrayList;


public class ClientDAL {
	public ClientDAL() {
		
	}

	public void insertClient(String name, int cnp) {
        try {

            Connection conn = DbConnection.getConnection();
            Statement stm = conn.createStatement();
            stm.execute("INSERT INTO client (cnp, name) VALUES ('" + cnp + "', '" + name + "')");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	public void updateClient(String name, int cnp) {
		try {

			Connection conn = DbConnection.getConnection();
			Statement stm = conn.createStatement();
			stm.execute("UPDATE client SET cnp = '" + cnp + "', name = '" + name + "' WHERE cnp = '" + cnp + "'" );

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteClient(int cnp) {
		try {

			Connection conn = DbConnection.getConnection();
			Statement stm = conn.createStatement();
			stm.execute("DELETE FROM client WHERE cnp = '" + cnp + "'");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Object[][] showClients(){
		Object[][] data = new Object[10][10];

		try {

			Connection conn = DbConnection.getConnection();
			Statement stm = conn.createStatement();
			stm.execute("SELECT * FROM client");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}
}