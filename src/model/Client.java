package model;

public class Client {
	
	private String name;
	private int cnp;
	
	public Client() {
		
	}
	
	public Client(String name, int cnp) {
		super();
		this.name = name;
		this.cnp = cnp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCnp() {
		return cnp;
	}

	public void setCnp(int cnp) {
		this.cnp = cnp;
	}
	
	

}
