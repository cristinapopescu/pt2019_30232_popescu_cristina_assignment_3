package model;

public class Order {
	
	protected int order_id;
	protected int product_id;
	protected int client_id;
	protected int quantity;

	public Order() {

	}
	public Order(int order_id, int product_id, int client_id, int quantity) {
		this.order_id = order_id;
		this.product_id = product_id;
		this.client_id = client_id;
		this.quantity = quantity;
	}

	public int getIdorder() {
		return order_id;
	}

	public void setIdorder(int order_id) {
		this.order_id = order_id;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public int getClient_id() {
		return client_id;
	}

	public void setClient_id(int client_id) {
		this.client_id = client_id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
