package ui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class ProductFrame extends JFrame {
	public JFrame frame = new JFrame();
	public JTextField productName = new JTextField();
	public JTextField productId = new JTextField();
	public JTextField productPrice = new JTextField();
	public JTextField productQuantity = new JTextField();
	public JButton addProduct = new JButton("Add");
	public JButton editProduct = new JButton("Edit");
	public JButton deleteProduct = new JButton("Delete");
	public JButton viewProduct = new JButton("View");
	
	public JTable table = new JTable();
	
	
	public void createFrame() {
		JPanel panel = new JPanel();
		
		JLabel id = new JLabel();
		JLabel name = new JLabel();
		JLabel quantity = new JLabel();
		JLabel price = new JLabel();

		JScrollPane pane = new JScrollPane(table);
		pane.setBounds(300, 10, 250, 500);
		
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setContentPane(panel);
		frame.setSize(600, 600);
		panel.setLayout(null);
		
		Object[] columns = {"ID", "Name"};
		DefaultTableModel model = new DefaultTableModel();
		model.setColumnIdentifiers(columns);
		table.setModel(model);
		table.setBackground(Color.cyan);
		table.setForeground(Color.white);
		Font font = new Font("", 1, 22);
		table.setFont(font);
		table.setRowHeight(30);
		
		id.setText("ID :");
		id.setSize(300, 40);
		id.setLocation(30, 20);
		panel.add(id);
		
		productId.setSize(200, 30);
		productId.setLocation(80, 20);
		panel.add(productId);
		
		name.setText("Name :");
		name.setSize(300, 40);
		name.setLocation(30, 100);
		panel.add(name);
		
		productName.setSize(200, 30);
		productName.setLocation(80, 100);
		panel.add(productName);

		price.setText("Price :");
		price.setSize(300, 40);
		price.setLocation(30, 180);
		panel.add(price);

		productPrice.setSize(200, 30);
		productPrice.setLocation(80, 180);
		panel.add(productPrice);

		quantity.setText("Quantity :");
		quantity.setSize(300, 40);
		quantity.setLocation(30, 260);
		panel.add(quantity);

		productQuantity.setSize(200, 30);
		productQuantity.setLocation(80, 260);
		panel.add(productQuantity);
		
		addProduct.setSize(100, 30);
		addProduct.setLocation(80, 370);
		panel.add(addProduct);
		
		editProduct.setSize(100, 30);
		editProduct.setLocation(80, 420);
		panel.add(editProduct);
		
		deleteProduct.setSize(100, 30);
		deleteProduct.setLocation(80, 470);
		panel.add(deleteProduct);
		
		viewProduct.setSize(100, 30);
		viewProduct.setLocation(80, 520);
		panel.add(viewProduct);
		
		panel.add(pane);
	}
}
