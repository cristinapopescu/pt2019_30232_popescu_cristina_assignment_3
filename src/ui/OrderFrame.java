package ui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class OrderFrame extends JFrame {
    public JFrame frame = new JFrame();
    public JTextField clientIDText = new JTextField();
    public JTextField productNameText = new JTextField();
    public JTextField quantity = new JTextField();
    public JButton createOrder = new JButton("Order");

    public void createFrame() {
        JPanel panel = new JPanel();

        JLabel cnp = new JLabel();
        JLabel name = new JLabel();
        JLabel quantityLabel = new JLabel();

        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        frame.setSize(400, 600);
        panel.setLayout(null);


        cnp.setText("CNP:");
        cnp.setSize(300, 40);
        cnp.setLocation(30, 20);
        panel.add(cnp);

        clientIDText.setSize(200, 30);
        clientIDText.setLocation(80, 20);
        panel.add(clientIDText);

        name.setText("Product:");
        name.setSize(300, 40);
        name.setLocation(30, 100);
        panel.add(name);

        productNameText.setSize(200, 30);
        productNameText.setLocation(80, 100);
        panel.add(productNameText);

        quantityLabel.setText("Quantity:");
        quantityLabel.setSize(300, 40);
        quantityLabel.setLocation(30, 180);
        panel.add(quantityLabel);

        quantity.setSize(200, 30);
        quantity.setLocation(80, 180);
        panel.add(quantity);

        createOrder.setSize(100, 30);
        createOrder.setLocation(80, 500);
        panel.add(createOrder);

    }
}
