package ui;

import bl.ClientServices;
import bl.OrderServices;
import bl.ProductServices;
import dal.*;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClientFrame clientFrame = new ClientFrame();
		ProductFrame productFrame = new ProductFrame();
		OrderFrame orderFrame = new OrderFrame();

		ClientServices clientServices = new ClientServices();
		ProductServices  productServices = new ProductServices();
		OrderServices orderServices = new OrderServices();

		clientFrame.createFrame();
		clientFrame.frame.setVisible(true);

		productFrame.createFrame();
		productFrame.frame.setVisible(true);

		orderFrame.createFrame();
		orderFrame.frame.setVisible(true);


		clientFrame.addClient.addActionListener(o1 -> {
			clientServices.addClient(Integer.parseInt(clientFrame.cnpText.getText()), clientFrame.nameText.getText());
			clientFrame.nameText.setText(null);
			clientFrame.cnpText.setText(null);

			clientFrame.revalidate();
			clientFrame.repaint();
		});

		clientFrame.deleteClient.addActionListener(o2 -> {
			clientServices.deleteClient(Integer.parseInt(clientFrame.cnpText.getText()));

			clientFrame.nameText.setText(null);
			clientFrame.cnpText.setText(null);

			clientFrame.revalidate();
			clientFrame.repaint();
		});

		clientFrame.editClient.addActionListener(o3 -> {
			clientServices.updateClient(Integer.parseInt(clientFrame.cnpText.getText()), clientFrame.nameText.getText());

			clientFrame.nameText.setText(null);
			clientFrame.cnpText.setText(null);

			clientFrame.revalidate();
			clientFrame.repaint();
		});

		productFrame.addProduct.addActionListener(o1 -> {
			productServices.addProduct(Integer.parseInt(productFrame.productId.getText()), productFrame.productName.getText(), Integer.parseInt(productFrame.productQuantity.getText()), Float.parseFloat(productFrame.productPrice.getText()));
			productFrame.productId.setText(null);
			productFrame.productName.setText(null);
			productFrame.productQuantity.setText(null);
			productFrame.productPrice.setText(null);

			productFrame.revalidate();
			productFrame.repaint();
		});

		productFrame.deleteProduct.addActionListener(o2 -> {
			productServices.deleteProduct(Integer.parseInt(productFrame.productId.getText()));
			productFrame.productId.setText(null);
			productFrame.productName.setText(null);
			productFrame.productQuantity.setText(null);
			productFrame.productPrice.setText(null);

			productFrame.revalidate();
			productFrame.repaint();
		});

		productFrame.editProduct.addActionListener(o3 -> {
			productServices.updateProduct(Integer.parseInt(productFrame.productId.getText()), productFrame.productName.getText(), Integer.parseInt(productFrame.productQuantity.getText()), Float.parseFloat(productFrame.productPrice.getText()));
			productFrame.productId.setText(null);
			productFrame.productName.setText(null);
			productFrame.productQuantity.setText(null);
			productFrame.productPrice.setText(null);

			productFrame.revalidate();
			productFrame.repaint();
		});

		productFrame.viewProduct.addActionListener(o4 -> {
			productServices.showProducts();
			productFrame.productId.setText(null);
			productFrame.productName.setText(null);
			productFrame.productQuantity.setText(null);
			productFrame.productPrice.setText(null);

			productFrame.revalidate();
			productFrame.repaint();
		});

		orderFrame.createOrder.addActionListener(o5 -> {

			orderServices.insertOrder(3, Integer.parseInt(orderFrame.productNameText.getText()),Integer.parseInt(orderFrame.clientIDText.getText()), Integer.parseInt(orderFrame.quantity.getText()));
		});
		}
}
