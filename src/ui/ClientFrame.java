package ui;

import bl.ClientServices;
import model.Client;
import java.lang.reflect.Field;

import java.awt.Color;
import java.awt.Font;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;


public class ClientFrame extends JFrame {

	private ClientServices clientServices = new ClientServices();
	public JFrame frame = new JFrame();
	public JTextField cnpText = new JTextField();
	public JTextField nameText = new JTextField();
	public JButton addClient = new JButton("Add");
	public JButton editClient = new JButton("Edit");
	public JButton deleteClient = new JButton("Delete");
	public JButton viewClient = new JButton("View");
	public JTable table = new JTable();


	public void createFrame() {
		JPanel panel = new JPanel();

		JLabel cnp = new JLabel();
		JLabel name = new JLabel();

		JScrollPane pane = new JScrollPane(table);
		pane.setBounds(300, 10, 250, 500);

		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setContentPane(panel);
		frame.setSize(600, 600);
		panel.setLayout(null);

		Object[] columns = {"ID", "Name"};
		DefaultTableModel model = new DefaultTableModel();
		model.setColumnIdentifiers(columns);
		table.setModel(model);
		table.setBackground(Color.cyan);
		table.setForeground(Color.white);
		Font font = new Font("", 1, 22);
		table.setFont(font);
		table.setRowHeight(30);

		cnp.setText("CNP :");
		cnp.setSize(300, 40);
		cnp.setLocation(30, 20);
		panel.add(cnp);

		cnpText.setSize(200, 30);
		cnpText.setLocation(80, 20);
		panel.add(cnpText);

		name.setText("Name :");
		name.setSize(300, 40);
		name.setLocation(30, 100);
		panel.add(name);

		nameText.setSize(200, 30);
		nameText.setLocation(80, 100);
		panel.add(nameText);

		addClient.setSize(100, 30);
		addClient.setLocation(80, 150);
		panel.add(addClient);

		editClient.setSize(100, 30);
		editClient.setLocation(80, 200);
		panel.add(editClient);

		deleteClient.setSize(100, 30);
		deleteClient.setLocation(80, 250);
		panel.add(deleteClient);

		viewClient.setSize(100, 30);
		viewClient.setLocation(80, 300);
		panel.add(viewClient);

		panel.add(pane);

	}
}
